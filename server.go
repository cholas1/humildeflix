package main

import ("log"
	"time"
	"net/http"
	"encoding/json")

var cebola *MovieController = &MovieController{}

type OnionHandler struct{}

func (OnionHandler) ServeHTTP(res http.ResponseWriter, req *http.Request) {


	if (req.Method == "POST" && req.URL.Path == "/movie") {
	      cebola.Create(req.Body)
	      jon, _ := json.Marshal(HTTPMessages[201])
	      res.Write(jon)
	      return
	}
	
	if (req.Method == "GET" && req.URL.Path == "/movie") {
	      movie := cebola.Search(req.URL.Query().Get("name"))
	      jon, _ := json.Marshal(movie)
	      res.Write(jon)
	      return
	}

	genre := req.URL.Query().Get("genre")
		
	if(!IsGenreValid(genre)) {
		jon, _ := json.Marshal(HTTPMessages[418])
		res.Write(jon)
		return
	}
	
	_, err := GenreTable[genre].CollectRatings()
	
	if(err != nil) {
		res.Write([]byte("Foi mal, não sei programar"))
		return
	}
	
	
	//res.Write()
}


func StartServer() {

	s := &http.Server{
		Addr:           "191.176.73.84",
		Handler:        OnionHandler{},
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
	}

	log.Fatal(s.ListenAndServe())
}




