package main

type DefaultMovie struct {

	Movie
}

func (DefaultMovie) String() (string) {
	return "Default"
}

func (DefaultMovie) CollectRatings() (float32, error) {
	return 2, nil
}
