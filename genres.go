package main

import "fmt"

var GenreTable = map[string]Searchable {
	"Super-Hero": SuperheroMovie{},
	"Cult": CultMovie{},
	"Default": DefaultMovie{},
	"Terror": TerrorMovie{},
}

func Run() {
	
	userInput := RequestUserInputAsString("Gender: ")
	
	if(!IsGenreValid(userInput)) {
		fmt.Println("Invalid Genre")
		return
	}
	
	rating, err := GenreTable[userInput].CollectRatings()
	
	if(err != nil) {
		fmt.Println("Something was wrong")
		return
	}
	
	fmt.Printf("Rating: %v  Error: %v", rating, err)
	
	
}

func RequestUserInputAsString(message string) (string) {
	var userInput string
	fmt.Println(message)
	fmt.Scan(&userInput)
	return userInput
}

func IsGenreValid(genre string) (bool) {
	return GenreTable[genre] != nil
}



	















   
