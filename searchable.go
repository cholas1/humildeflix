package main

type Searchable interface {
	
	CollectRatings() (float32, error);
}
