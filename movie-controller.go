package main

import ("io"
	"encoding/json")

type MovieController struct {
	Movies []Movie
}

func (mc *MovieController) Search(name string) Movie {
	for i := 0; i < len(mc.Movies); i++ {
		if(name == mc.Movies[i].Title) {
			return mc.Movies[i]
		}	
	}
	
	return Movie{}
}

func (mc *MovieController) Create(body io.ReadCloser) error {
	var m Movie
	
	err := json.NewDecoder(body).Decode(&m)
	
	if err != nil {
		return err
	}
	
	mc.Movies = append(mc.Movies, m)
	
	return nil
}
